@extends('layouts.admin-master')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-8">
            <a href="{{Route($route.'.index')}}" class="mb-3 btn btn-info">Back</a>
            @if($errors->any())
                <div class="alert alert-danger" role="alert">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <strong>Form Edit {{$page}} : {{$$model->title}}</strong>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="post" action="{{Route($route.'.update',$$model->id)}}" enctype="multipart/form-data">
                        @csrf
                        @if($$model->file != null)
                        <div class="form-group">
                            <img src="{{asset('uploads/'.$$model->file)}}" class="img-fluid" />
                        </div>
                        @endif
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" class="form-control" name="title" value="{{$$model->title ? $$model->title : old('title')}}">
                        </div>
                        <div class="form-group">
                            <label>Content</label>
                            <textarea class="form-control" name="content" >{{ $$model->content ? $$model->content : old('content')}}</textarea>
                        </div>
                        <!-- 
                        <div class="form-group">
                            <label>File</label>
                            <input type="file" class="form-control" name="file">
                        </div> -->
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Save">
                        </div>
                        

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
