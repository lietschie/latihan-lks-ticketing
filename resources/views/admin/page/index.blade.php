@extends('layouts.admin-master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{$page}}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <a href="{{Route($route.'.create')}}" class="mb-3 btn btn-primary">Create</a>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Content</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($$model as $data)
                                <tr>
                                    <td>{{$data->title}}</td>
                                    <td>{{$data->content}}</td>
                                    <td>
                                        <a href="{{Route($route.'.edit',$data->id)}}" class="btn btn-warning">Edit</a>
                                        <a href="{{Route($route.'.delete',$data->id)}}" class="btn btn-danger">Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
