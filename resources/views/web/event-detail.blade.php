@extends('layouts.web-master')

@section('content')
<section class="p-4">
    <div class="row ">
        <div class="col">

        </div>
        <div class="col-md-8 align-self-center">
            <div class="card">
                <img src="{{asset('uploads/'.$event->file)}}" class="card-img-top img-fluid " alt="...">
                <div class="card-body">
                    <h2 class="card-title">{{$event->title}}</h2>
                    <hr>
                    <div class="row">
                        <div class="col-md-8 border-right" >
                            <p>{{$event->content}}</p>

                        </div>

                        <div class="col-md-4">
                            Date : {{$event->event_date}}
                        </div>
                    </div>
                    

                </div>

            </div>
        </div>
        <div class="col">

        </div>

    </div>

    <div class="row ">
        <div class="col-md-8 d-flex justify-content-center">



        </div>

    </div>
</section>

<section class="container py-4">

</section>

@endsection
