@extends('layouts.web-master')

@section('content')
<!-- Section Banner -->
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        @foreach($banner as $b)
        <div class="carousel-item {{$loop->first ? 'active' : '' }}">
            <img src="{{asset('uploads/'.$b->file)}}" class="d-block  w-100" style="max-height:600px:height:auto;" alt="...">
        </div>
        @endforeach
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<section class="container py-4">
    <h2 class="font-weight-bold text-monospace">Feature Event</h2>

    @foreach($featured as $e)
    <div class="card mb-3">
        <div class="card-body">
            <div class="row">

                <div class="col-md-6">
                    <img src="{{asset('uploads/'.$e->file)}}" class="img-fluid" />
                </div>
                <div class="col-md-6">
                    <h2>{{$e->title}}</h2>
                    <h2><small class="text-muted">Jakarta</small></h2>
                    <p class="card-text">Date : {{$e->event_date->toDayDateTimeString()}}</p>
                    <p class="card-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptas non aliquid
                        qui, molestiae, dolore harum ullam commodi veritatis minus ea amet repudiandae delectus dolores
                        nihil ipsam adipisci nostrum, dolorum libero!</p>
                </div>
            </div>
        </div>
    </div>

    @endforeach
</section>

<!-- Section Featured -->
<section class="container py-4">
    <h2 class="font-weight-bold text-monospace">Discover Events</h2>

    <div class="row">
        @foreach($event as $f)
        <div class="col-md-4">

            <div class="card mb-3" style="min-height:400px">
                <img src="{{asset('uploads/'.$f->file)}}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h4 class="card-title">
                        <strong>
                            {{$f->title}}
                        </strong>
                        <!-- <a href="#" class="btn btn-light float-right">See</a> -->
                    </h4>
                    <p class="card-text">{{$f->event_date->toDayDateTimeString()}}</p>
                    <p class="card-text">
                        <strong>
                            {{$f->ticket_price == 0 ? 'Free' : number_format($f->ticket_price,0,',','.')}}
                        </strong>
                    </p>

                </div>
            </div>

        </div>
        @endforeach
    </div>

</section>
<!-- See More Event -->
<section class="mb-5">
    <a href="#">
        <h2 class="font-weight-bold  text-center">
            <u>See more events</u>
        </h2>
    </a>
</section>
<!-- Normal Event -->

@endsection
