@extends('layouts.web-master')

@section('content')
<!-- <br> -->
<section class="container py-4">
    <h2 class="font-weight-bold text-monospace">Discover Events</h2>

    <div class="row">
        @foreach($event as $e)
        <div class="col-md-4">
            <div class="card mb-3" style="min-height:400px">
                <a href="{{Route('web.event.detail',$e->id)}}">
                    <img src="{{asset('uploads/'.$e->file)}}" class="card-img-top" alt="...">
                </a>

                <div class="card-body">
                    <h4 class="card-title">
                        <strong>
                            <a href="{{Route('web.event.detail',$e->id)}}">
                                {{$e->title}}
                            </a>
                        </strong>
                        <!-- <a href="#" class="btn btn-light float-right">See</a> -->
                    </h4>
                    <p class="card-text">{{$e->event_date->toDayDateTimeString()}}</p>
                    <p class="card-text">
                        <strong>
                            {{$e->ticket_price == 0 ? 'Free' : "Rp.".number_format($e->ticket_price,0,',','.')}}
                        </strong>
                    </p>

                </div>
            </div>

        </div>
        @endforeach
    </div>

</section>
@endsection
