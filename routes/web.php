<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/','WebController@index')->name('web.index');
Route::get('/event','WebController@event')->name('web.event');
Route::get('/event/{id}','WebController@eventDetail')->name('web.event.detail');
Route::get('/about','WebController@about')->name('web.about');
Route::get('/contact','WebController@contact')->name('web.contact');

Route::prefix('customer')->namespace('Customer')->middleware('auth')->group(function(){
    //Default User Route
    Route::get('/','DashboardController@index')->name('user.index');
});


Route::prefix('admin')->namespace('Admin')->middleware(['auth','admin'])->group(function(){
    //Default Admin Route
    Route::get('/','DashboardController@index')->name('admin.index');
    //Event Group Route
    Route::prefix('event')->group(function(){
        Route::get('/','EventController@index')->name('admin.event.index');
        Route::get('/create','EventController@create')->name('admin.event.create');
        Route::post('/store','EventController@store')->name('admin.event.store');
        Route::get('/{id}/edit','EventController@edit')->name('admin.event.edit');
        Route::post('/{id}/update','EventController@update')->name('admin.event.update');
        Route::get('/{id}/show','EventController@show')->name('admin.event.show');
        Route::get('/{id}/delete','EventController@delete')->name('admin.event.delete');
    });
    //Banner Group Route
    Route::prefix('banner')->group(function(){
        Route::get('/','BannerController@index')->name('admin.banner.index');
        Route::get('/create','BannerController@create')->name('admin.banner.create');
        Route::post('/store','BannerController@store')->name('admin.banner.store');
        Route::get('/{id}/edit','BannerController@edit')->name('admin.banner.edit');
        Route::post('/{id}/update','BannerController@update')->name('admin.banner.update');
        Route::get('/{id}/show','BannerController@show')->name('admin.banner.show');
        Route::get('/{id}/delete','BannerController@delete')->name('admin.banner.delete');
    });
    //Page Group Route
    Route::prefix('page')->group(function(){
        Route::get('/','PageController@index')->name('admin.page.index');
        Route::get('/create','PageController@create')->name('admin.page.create');
        Route::post('/store','PageController@store')->name('admin.page.store');
        Route::get('/{id}/edit','PageController@edit')->name('admin.page.edit');
        Route::post('/{id}/update','PageController@update')->name('admin.page.update');
        Route::get('/{id}/show','PageController@show')->name('admin.page.show');
        Route::get('/{id}/delete','PageController@delete')->name('admin.page.delete');
    });
});