<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //

    public $table = 'event';

    public $fillable = [
        'title',
        'event_date',
        'content',
        'ticket_stock',
        'feature',
        'file',
        'ticket_price',
    ];

    public $dates = [
        'event_date'
    ];


}
