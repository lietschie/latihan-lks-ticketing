<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    
    public function __construct(){
        $this->page = 'Customer Dashboard';
        $this->model = 'Customer Dashboard';
        // $this->entity = new Event();
        $this->route = 'customer';
        $this->view = 'customer';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['model'] = $this->model;
        // $data[$this->model] = $this->entity->orderBy('id','desc')->get();
        $data['route'] = $this->route;
        $data['page'] = $this->page;
        return view($this->view.'.index',$data);
    }

    
}
