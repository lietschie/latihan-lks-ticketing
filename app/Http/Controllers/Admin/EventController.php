<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;
//Models
use App\Event;

class EventController extends Controller
{

    public function __construct(){
        $this->page = 'Event';
        $this->model = 'Event';
        $this->entity = new Event();
        $this->route = 'admin.event';
        $this->view = 'admin.event';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['model'] = $this->model;
        $data[$this->model] = $this->entity->orderBy('id','desc')->get();
        $data['route'] = $this->route;
        $data['page'] = $this->page;
        return view($this->view.'.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['route'] = $this->route;
        $data['page'] = $this->page;
        return view($this->view.'.create',$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request->all());
        $validatedData = $request->validate([
            'title' => 'required',
            'event_date' => 'required',
            'content' => 'nullable',
            'ticket_stock' => 'nullable|numeric',
            'feature' => 'nullable',
            'ticket_price' => 'required',
            
        ]);
        //
        $data = [
            'title' => $request->title,
            'event_date' => $request->event_date,
            'content' => $request->content,
            'ticket_stock' => $request->ticket_stock,
            'feature' => $request->feature,
            'ticket_price' => $request->ticket_price,
            
        ];
        if($request->file){
            $data['file'] = $request->file('file')->store('','custom');
        }

        $createEvent = $this->entity->create($data);
        

        return redirect()->route($this->route.'.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view($this->view.'.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data['model'] = $this->model;
        $data[$this->model] = $this->entity->findOrFail($id);
        $data['route'] = $this->route;
        $data['page'] = $this->page;
        return view($this->view.'.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $validatedData = $request->validate([
            'title' => 'required',
            'event_date' => 'required',
            'content' => 'nullable',
            'ticket_stock' => 'nullable|numeric',
            'feature' => 'nullable',
            'ticket_price' => 'required',
            
        ]);

        $getEvent = $this->entity->findOrFail($id);
        //
        $data = [
            'title' => $request->title,
            'event_date' => $request->event_date,
            'content' => $request->content,
            'ticket_stock' => $request->ticket_stock,
            'feature' => $request->feature??0,
            'ticket_price' => $request->ticket_price,
            
        ];
        if($request->file){
            $checkFile = Storage::disk('custom')->exists($getEvent->file);
            if($checkFile){
                $deleteFile = Storage::disk('custom')->delete($getEvent->file);
            }
            $data['file'] = $request->file('file')->store('','custom');
        }

        $createEvent = $getEvent->update($data);
        return redirect()->route($this->route.'.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $getEvent = $this->entity->findOrFail($id);
        $checkFile = Storage::disk('custom')->exists($getEvent->file);
        if($checkFile){
            $deleteFile = Storage::disk('custom')->delete($getEvent->file);
        }
        $getEvent->delete();
        
        return redirect()->route($this->route.'.index');
    }
}
