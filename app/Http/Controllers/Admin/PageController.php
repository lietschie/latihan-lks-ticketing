<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//Models
use App\Page;
use Storage;

class PageController extends Controller
{
    public function __construct(){
        $this->page = 'Page';
        $this->model = 'Page';
        $this->entity = new Page();
        $this->route = 'admin.page';
        $this->view = 'admin.page';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['model'] = $this->model;
        $data[$this->model] = $this->entity->get();
        $data['route'] = $this->route;
        $data['page'] = $this->page;
        return view($this->view.'.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['route'] = $this->route;
        $data['page'] = $this->page;
        return view($this->view.'.create',$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'title' => 'required',
            'content' => 'nullable',
        ]);
        //
        $data = [
            'title' => $request->title,
            'content' => $request->content,
        ];
        if($request->file){
            $data['file'] = $request->file('file')->store('','custom');
        }

        $createPage = $this->entity->create($data);
        

        return redirect()->route($this->route.'.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view($this->view.'.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data['model'] = $this->model;
        $data[$this->model] = $this->entity->findOrFail($id);
        $data['route'] = $this->route;
        $data['page'] = $this->page;
        return view($this->view.'.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $validatedData = $request->validate([
            'title' => 'required',
            'content' => 'nullable',
        ]);

        $getPage = $this->entity->findOrFail($id);
        //
        $data = [
            'title' => $request->title,
            'content' => $request->content,
        ];
        if($request->file){
            $checkFile = Storage::disk('custom')->exists($getPage->file);
            if($checkFile){
                $deleteFile = Storage::disk('custom')->delete($getPage->file);
            }
            $data['file'] = $request->file('file')->store('','custom');
        }

        $createPage = $getPage->update($data);
        return redirect()->route($this->route.'.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $getPage = $this->entity->findOrFail($id);
        $checkFile = Storage::disk('custom')->exists($getPage->file);
        if($checkFile){
            $deleteFile = Storage::disk('custom')->delete($getPage->file);
        }
        $getPage->delete();
        
        return redirect()->route($this->route.'.index');
    }
}
