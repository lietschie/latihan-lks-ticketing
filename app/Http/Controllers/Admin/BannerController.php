<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//Modals
use App\Banner;
//Storage
use Storage;
class BannerController extends Controller
{
    public function __construct(){
        $this->page = 'Banner';
        $this->model = 'Banner';
        $this->entity = new Banner();
        $this->route = 'admin.banner';
        $this->view = 'admin.banner';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['model'] = $this->model;
        $data[$this->model] = $this->entity->get();
        $data['route'] = $this->route;
        $data['page'] = $this->page;
        return view($this->view.'.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['route'] = $this->route;
        $data['page'] = $this->page;
        return view($this->view.'.create',$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'file' => 'required',
        ]);
        
        if($request->file){
            $data['file'] = $request->file('file')->store('banner','custom');
        }

        $createBanner = $this->entity->create($data);
        return redirect()->route($this->route.'.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view($this->view.'.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data['model'] = $this->model;
        $data[$this->model] = $this->entity->findOrFail($id);
        $data['route'] = $this->route;
        $data['page'] = $this->page;
        return view($this->view.'.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $validatedData = $request->validate([
            'file' => 'required',
        ]);

        $getBanner = $this->entity->findOrFail($id);
        if($request->file){
            $checkFile = Storage::disk('custom')->exists($getBanner->file);
            if($checkFile){
                $deleteFile = Storage::disk('custom')->delete($getBanner->file);
            }
            $data['file'] = $request->file('file')->store('banner','custom');
        }

        $createBanner = $getBanner->update($data);
        return redirect()->route($this->route.'.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $getBanner = $this->entity->findOrFail($id);
        $checkFile = Storage::disk('custom')->exists($getBanner->file);
        if($checkFile){
            $deleteFile = Storage::disk('custom')->delete($getBanner->file);
        }
        $getBanner->delete();
        
        return redirect()->route($this->route.'.index');
    }
}
