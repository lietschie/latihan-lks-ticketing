<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;
use App\Event;
class WebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['banner'] = Banner::get();
        $data['featured'] = Event::where('feature',1)->orderBy('id','desc')->limit(3)->get();
        $data['event'] = Event::where('feature',0)->get();

        return view('web.index',$data);
    }

    public function event()
    {
        $data['event'] = Event::paginate(6);
        return view('web.event',$data);
    }

    public function eventDetail($id)
    {
        $data['event'] = Event::findOrFail($id);
        return view('web.event-detail',$data);
    }
}
